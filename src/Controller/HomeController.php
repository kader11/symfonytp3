<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',

            'thesis' =>  [
                ['titre'=>'Econometrie et statistique','contact'=>'kaderyousfi@gmail.fr','Description' =>'recherche sur la methode des moinde carre ordinaire'], 
                ['titre'=>'Modélisation_linaire et non linaire','contact'=>'kader@gmail.fr', 'Description' => ' Estimations des paramètres et somme résiduelle des carrés. Pour chaque modèle '],
                [ 'titre'=> 'simulation  monte carlo','contact'=> 'yousfi@yahoo.fr','Description' => 'Introduction au modele monte carlo']
            ] 
        ]);
    }
}
